/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hometaskFirm;

/**
 *
 * @author sergey
 */
import java.io.Serializable;
import java.util.*;
public class Firm  implements Serializable {
  
  public Firm (Double account){

    this.account = account;
    this.employees = new <Employee> ArrayList();
    this.departments = new <Department> ArrayList();
    this.addDepartment(new Department("Новый отдел"));
    this.addDepartment(new Department("Отдел продаж"));
  }
  
  public void addEmployee (Employee employee){
      if(!employee.equals(null)){
          this.dId++;
          employee.setId(this.dId);
          this.employees.add(employee);
      }

  }
  
  public void addDepartment (Department department){
    if(!department.equals(null)){
        this.eId++;
        department.setId(this.eId);
        this.departments.add(department);
    }

  }

  public ArrayList <Department> getDepartments(){
      return this.departments;
  }

  public boolean issetDepartment(int id){
      for (Department department: this.departments){
        if(department.getId().equals(id))
            return true;
      }
      return false;
  }

  public ArrayList <Manager> getManagers(){
      ArrayList <Manager> list = new <Manager> ArrayList();

      for (Employee manager: this.employees){
          if(manager instanceof Manager)
              list.add((Manager) manager);
      }
      return list;
  }

    public ArrayList <SallesManager> getSallesManagers(){
        ArrayList <SallesManager> list = new <SallesManager> ArrayList();

        for (Employee manager: this.employees){
            if(manager instanceof SallesManager)
                list.add((SallesManager) manager);
        }
        return list;
    }

  public ArrayList <Employee> getEmployees(){
    return this.employees;
  }

  public void setEmployees(ArrayList <Employee> employes){
      this.employees = employes;
  }

  public Department getDepartment (int id){
      for (Department department: this.departments){
          if(department.getId().equals(id))
              return department;
      }
      return null;
  }

  public Double getBalanse(){
      return this.account;
  }
  public Employee getEmployee(int id){
      for(Employee employee: this.employees){
          if(employee.getId().equals(id))
              return employee;
      }
      return null;
  }
  public void issueSalary(){
      for (Employee employee : this.employees){
          Double salary = employee.getSalary();

          if(this.account - salary < 0){
             salary = Math.abs(this.account - salary);
          }

          employee.setSalaryAccount(salary);
          this.account-=salary;
      }

  }
  private ArrayList <Department>  departments;
  private ArrayList <Employee> employees;
  private Double account;
  private Integer eId = 0;
  private Integer dId = 0;

}
