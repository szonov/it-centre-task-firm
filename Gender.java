/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hometaskFirm;

/**
 *
 * @author sergey
 */
public enum Gender {
  MALE("М"), FEMALE("Ж"), UNKNOWN("хз");
  
  private Gender (String abbreviation){
    this.abbreviation = abbreviation;
  }
  
  public String getAbbreviation(){
    return this.abbreviation;
  }
  private String abbreviation;
}
