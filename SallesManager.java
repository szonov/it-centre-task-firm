/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hometaskFirm;

import java.util.Date;

/**
 *
 * @author sergey
 */
public class SallesManager extends Employee {

  public SallesManager (String aName, String aPatronymic, String aSurname, Gender aGender, Date aBirthday, Double aSalary){
      super(aName, aPatronymic, aSurname, aGender, aBirthday, aSalary );
  }

    @Override
    public void setSalaryAccount (Double salary){
        Double salaryAccount = this.getSlaryAccount();
        salaryAccount+= salary;
    }

    @Override
    public Double getSalary(){
        Double salary = super.getSalary();
        if(this.summSalles > 0){
            salary+= this.summSalles * 0.3;
        }
        this.summSalles = 0.0;
        return salary;
    }

    public void sale(Double sale){
      this.summSalles+= sale;
    }

    private Double summSalles = 0.0;
}
