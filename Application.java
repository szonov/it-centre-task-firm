/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hometaskFirm;

/**
 *
 * @author sergey
 */
import java.io.IOException;
import java.util.*;

public final class Application {
  
  public Application(){
    this.firm = Utils.load(this.fileStorage);
    this.printMenu();
  }
  
  public void printMenu() {
     System.out.println(this.menu);
     while(true){
      try {
        int command = Integer.parseInt(this.scan.nextLine());
        this.selectMenu(command);
        break;
      } catch (NumberFormatException e) {
      System.out.println("Wrong command");
      try{
          Runtime.getRuntime().exec("clear");
      }catch(IOException err){ }
      System.out.println(this.menu);
     }
    }
     
     
  }
  
  private void selectMenu (int index){
    switch (index){
      case 1:
          this.createEmployee();
        break;
      case 2:
          this.createDepartment();
        break;
      case 3:
           this.addEmployeesManager(0);
        break;
      case 4:
        this.printEmployees();
        this.sort();
       break;
      case 5:
          this.issueSalary();
        break;
      case 6:
            System.out.println(this.actionsMenu);
            try{
                int command = Integer.parseInt(this.scan.nextLine());
                this.actions(command);
            } catch (NumberFormatException e) { System.out.println("Wrong command"); }
            break;
      case 7:
          System.out.println("Введите номер менеджера");
          try{
              int command = Integer.parseInt(this.scan.nextLine());
              this.printManagerSubs(command);
          } catch (NumberFormatException e) { System.out.println("Wrong command"); }
          break;
      case 10:
          this.printBalanse();
        break;

      case 0:
          System.exit(0);
        break;
      default:
        System.out.println("Wrong command");
    } 
    this.printMenu();
  }

  private void sort(){
      System.out.println(this.menuSort);
      try{
        this.sort(Integer.parseInt(this.scan.nextLine()));
      }  catch (NumberFormatException e){
          this.sort();
      }
  }
  private void sort(int sort){
      switch (sort){

          case 1:
              this.salalarySort(true);
            break;
          case 2:
              this.salalarySort(false);
              break;
          case 0:
              break;
          default:
              this.sort();
      }
  }

  private void salalarySort(boolean asc){
      Employee temp;
      ArrayList <Employee> list = this.firm.getEmployees();
       for (int i = 0; i < list.size(); i++){
           for(int j = i+1; j < list.size(); j++){
               if(asc){
                   if(list.get(i).getSalary() > list.get(j).getSalary()){
                      temp = list.get(j);
                      list.set(j, list.get(i));
                      list.set(i,temp);
                   }
               }   else{
                   if(list.get(i).getSalary() < list.get(j).getSalary()){
                       temp = list.get(j);
                       list.set(j, list.get(i));
                       list.set(i,temp);
                   }
               }
           }
       }
      this.firm.setEmployees(list);
      this.printEmployees();
  }

  private void printManagerSubs(int id){
      Employee manager = this.firm.getEmployee(id);
      if(!(manager instanceof Manager)){
          System.out.println("Такого менеджера не существует");
          return;
      }

      Utils.printTableForEmployees(((Manager) manager).getSubordinates());

  }
  private void addEmployeesManager(int deep){
      if(deep > 3)
          return;
      try{
          System.out.println("Введите номер менеджера");
          int mid = Integer.parseInt(this.scan.nextLine());
          System.out.println("Введите номер сотрудника которого закрепить");
          int atach = Integer.parseInt(this.scan.nextLine());
          if(mid == atach){
              System.out.println("Менеджер не может быть своим подчиненым");
              this.addEmployeesManager(deep++);
          }
          Employee manager = this.firm.getEmployee(mid);

          Employee emloyee = this.firm.getEmployee(atach);

          if(!(manager instanceof Manager)){
              System.out.println("Такого менеджера не существует");
              this.addEmployeesManager(deep++);
          }

          if(!(emloyee instanceof Employee)){
              System.out.println("Такого сотрудника не существует");
              this.addEmployeesManager(deep++);
          }

          ((Manager) manager).addSubordinates(emloyee);
          Utils.save(this.firm, this.fileStorage);
      }  catch (NumberFormatException e){
           this.addEmployeesManager(deep++);
      }
  }
  private void actions(int command){
    switch (command){
        case 1:
            this.saleProducts();
          break;
        case 2:
            this.raiseSalaries();
            break;
        case  3:
            this.calcVocation();
            break;
        case 0:
            break;
        default:
            System.out.println("Wrong command");
    }
  }

    private void  calcVocation(){
         //
    }
  private void raiseSalaries(){
      ArrayList <Employee> employees = this.firm.getEmployees();
      int currentYear = Calendar.getInstance().get(Calendar.YEAR);
      Calendar calendar = Calendar.getInstance();
      for(Employee employee: employees){
          calendar.setTime(employee.getExperience());
          if(currentYear-(calendar.get(Calendar.YEAR)) >=5){
              employee.raiseSalary(employee.getSalary()*0.1);
          }
      }
      Utils.save(this.firm, this.fileStorage);
  }
  private void saleProducts(){
      ArrayList <SallesManager> sm = this.firm.getSallesManagers();

      if(sm.size() == 0){
          System.out.println("Нет ни одного продовца");
      }else{
         try{
            System.out.println("Введите сумму продажи");
            Double summ = Double.parseDouble(this.scan.nextLine());
            for (SallesManager sallesm : sm){
                sallesm.sale(summ);
            }
            Utils.save(this.firm, this.fileStorage);
         }catch (NumberFormatException e) {
           this.saleProducts();
         }

      }
  }
  private void issueSalary(){
      this.firm.issueSalary();
      Utils.save(this.firm, this.fileStorage);
      System.out.println("Зарплата выдана");
  }

  private void printBalanse(){
      System.out.println("Баланс фирмы: "+this.firm.getBalanse());
  }

  private void printEmployees(){
      ArrayList <Employee> employees =  this.firm.getEmployees();
      Utils.printTableForEmployees(employees);
  }


  private void createDepartment(){
      String name;
      while (true){
          System.out.println("Введите название отдела фирмы");
          name = this.scan.nextLine();
          if(name.length() > 0)
              break;
      }
      this.firm.addDepartment(new Department(name));
  }

  private void createEmployee(){

      int type, year, month, date;
      int dep =0;
      double salary;
      Gender g = Gender.UNKNOWN;
      String name, patronymic, surname, gender;
      Employee employee = null;

      while (true){
          System.out.println("Введите имя сотрудника");
          name = this.scan.nextLine();

          if(name.length() > 0){
              break;
          }
      }
      while(true){
          System.out.println("Введите отчество сотрудника");
          patronymic = this.scan.nextLine();
          if(patronymic.length() > 0){
              break;
          }
      }

      while(true){
          System.out.println("Введите фамилию сотрудника");
          surname = this.scan.nextLine();
          if(surname.length() > 0){
              break;
          }
      }

      while(true){
          System.out.println("Введите пол сотрудника (м,ж)");
          gender = this.scan.nextLine();
          if(gender.toLowerCase().equals("м") || gender.toLowerCase().equals("ж") ){
              break;
          }
      }


      int currentYear = Calendar.getInstance().get(Calendar.YEAR);

      while(true){
          System.out.println("Введите год рождения");
          try {
              year = Integer.parseInt(this.scan.nextLine());
              if(year > 1940 && year <= currentYear - 18 )
                break;
          } catch (NumberFormatException e) { }
      }

      while(true){
          System.out.println("Введите месяц рождения");
          try {
              month = Integer.parseInt(this.scan.nextLine());
              if(month >= 1 && month <= 12 )
                  break;
          } catch (NumberFormatException e) { }
      }

      while(true){
          System.out.println("Введите день рождения");
          try {
              date = Integer.parseInt(this.scan.nextLine());
              if(date >= 1 && date <= 31 )
                  break;
          } catch (NumberFormatException e) { }
      }

      while(true){
          System.out.println("Введите зарплату сотрудника");
          try {
              salary = Double.parseDouble(this.scan.nextLine());
              if( salary > 0 )
                  break;
          } catch (NumberFormatException e) { }
      }

      while(true){
          System.out.println("Введите должность сотрудника\n 1 Менеджер \n 2 Продавец \n 3 Обычный сотрудник");
          try {
              type = Integer.parseInt(this.scan.nextLine());
              if( type >= 1 && type <= 3 )
                  break;
          } catch (NumberFormatException e) { }
      }


     while(true){
         System.out.println("Введите номер отдела");
         ArrayList <Department> departments = this.firm.getDepartments();
         for (Department department : departments){
             System.out.println(department.getId()+" "+department.getName());
         }
         try {
             dep = Integer.parseInt(this.scan.nextLine());
             if( this.firm.issetDepartment(dep) )
                 break;
         } catch (NumberFormatException e) { }
     }

     Date birthday;
     Calendar cal = GregorianCalendar.getInstance();
     cal.set(year, month, date);
     birthday = cal.getTime();

     if(gender.equals("м")){
         g = Gender.MALE;
     }else if(gender.equals("ж")){
         g = Gender.FEMALE;
     }

     switch (type){
         case 1:
            Department department = this.firm.getDepartment(dep);
            employee = new Manager (name, patronymic, surname, g, birthday, salary, department);
            break;
         case 2:
             employee = new SallesManager (name, patronymic, surname, g, birthday, salary);
             break;
         case 3:
             employee = new Employee (name, patronymic, surname, g, birthday, salary);
             break;
     }

     this.firm.addEmployee(employee);

     Utils.save(this.firm, this.fileStorage);
  }

  private Scanner scan = new Scanner(System.in);
  private Firm firm;
  private String fileStorage = "serializeble.txt";
  private String menu = "1 Создать сотрудника\n"
                      + "2 Создать отдел\n"
                      + "3 Добавить менеджеру подчиненных\n"
                      + "4 Вывести всех сотрудников\n"
                      + "5 Выдать всем зарплату\n"
                      + "6 Действия\n"
                      + "7 Вывести сотрудников закрепленных за менеджером\n"
                      + "10 Баланс фирмы\n"
                      + "0 Выход";
  private String menuSort = "1 Зарплата по возрастанию\n"
                            +"2 Зарплата по убыванию\n"
                            +"0 В гланое меню\n";
  private String actionsMenu = "1 Продать продовцам товар\n"
                             + "2 Поднять жаловние на 10% \n"
                             + "3 Пересчитать отпуска \n"
                             + "4 Выдать праздничный бонус \n"
                             + "5 Нанять сотрудника \n"
                             + "6 Уволить сотрудников \n"
                             + "7 Выдать бонус\n"
                             + "8 Поднять жалование\n"
                             + "9 Пересчитать отпуска\n"
                             + "0 В главное меню\n";

}
