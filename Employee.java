/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hometaskFirm;

/**
 *
 * @author sergey
 */
import java.io.Serializable;
import java.util.*;

public class Employee implements Serializable {
  
  public Employee(String aName, String aPatronymic, String aSurname, Gender aGender, Date aBirthday, Double aSalary){
    this.name = aName;
    this.patronymic = aPatronymic;
    this.surname = aSurname;
    this.gender = aGender; 
    this.salary = aSalary;
    this.birhtday = aBirthday;
  }
  
  public String getName(){
    return this.name;
  }
  
  public String getPatronymic (){
    return this.patronymic;
  }
  
  public String getSurname(){
    return this.surname;
  }
  
  public Gender getGender(){
    return this.gender;
  }
  
  public Date getBirthday(){
    return this.birhtday;
  }
  
  public Double getSalary(){
    return this.salary;
  }
  
  public Double getSlaryAccount(){
    return this.salaryAccount;
  }
  
  public Date getExperience (){
    return this.experience;
  }

  public void setSalaryAccount (Double salary){
     this.salaryAccount+= salary;
  }

  public void setId(Integer id){
      this.ID = id;
  }
  public Integer getId(){
     return this.ID;
  }
  @Override
  
  public String toString(){
    return this.getClass().getName()+" [gender=" + gender
           + ", name=" + name
           + ", patronymic=" + patronymic
           + ", surname=" + surname
           + ", birhtday=" + birhtday.toString()
           + ", salary=" + salary 
           + ", salaryAccount=" + salaryAccount 
           + ", experience=" + experience.toString()
           + "]";
  }

   public void raiseSalary (Double add){
      this.salary+=add;
    }

  private Integer ID;
  private Gender gender;
  private String name;
  private String patronymic;
  private String surname;
  private Date birhtday;
  private Double salary;
  private Double salaryAccount=0.0;
  private Date experience;
  private Integer vocation;

    {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        int currentMont = Calendar.getInstance().get(Calendar.MONTH);
        int currentDay = Calendar.getInstance().get(Calendar.DATE);
        Random rand = new Random();

        int randomYear = rand.nextInt((currentYear - 1980) + 1) + 1980;

        Calendar cal = GregorianCalendar.getInstance();
        cal.set(randomYear, (int)(Math.random()*currentMont+1), (int)(Math.random()*currentDay+1));
        this.experience = cal.getTime();
    }


}
