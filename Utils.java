package hometaskFirm;

import java.io.*;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: sergey
 * Date: 10/23/13
 * Time: 6:11 PM
 * To change this template use File | Settings | File Templates.
 */
public class Utils {
    public static void printTableForEmployees(ArrayList <Employee> employees){
        if(employees.size() == 0){
            System.out.println("Нет сотрудников");
            return;
        }

        int nameLength = 0;
        int salaryLength = 0;

        String FIO = null;
        int salary;
        for (Employee employee: employees){
             FIO = employee.getName()+" "+employee.getSurname()+" "+employee.getPatronymic();
             salary = Double.toString(employee.getSalary()).length() ;
            if(FIO.length() > nameLength)
                nameLength = FIO.length();

            if(salary > salaryLength)
                salaryLength = salary;
        }

        nameLength+=2;
        salaryLength+=12;

        StringBuffer borderName = new StringBuffer();
        StringBuffer nameFiled = new StringBuffer();

        for (int i = 0; i < nameLength; i++){
            borderName.append('-');

            if(i >2)
                nameFiled.append(" ");

            if(i == nameLength/2 ){
                nameFiled.append("ФИО");
            }


        }

        StringBuffer borderSalary = new StringBuffer();
        StringBuffer salarayFiled = new StringBuffer(" Зарплата");

        for (int i = 0; i < salaryLength; i++){
            borderSalary.append('-');
            if(i > 8)
            salarayFiled.append(" ");


        }

        String newLineMark = System.getProperty("line.separator");
        String leftAlignFormat = "| %-5d | %-"+(nameLength-2)+"s | %-6s | %-13s | %-"+(salaryLength-2)+".2f |" + newLineMark;

        System.out.format("+-------+"+borderName.toString()+"+--------+---------------+"+borderSalary.toString()+"+"  + newLineMark);
        System.out.printf("|   №   |" + nameFiled.toString() + "|  Пол   |       Тип     |"+salarayFiled.toString()+"|" + newLineMark);
        System.out.format("+-------+" + borderName.toString() + "+--------+---------------+"+borderSalary.toString()+"+" + newLineMark);

        for (Employee employee : employees) {
            String type = "Не известно";
            if(employee instanceof Manager){
                type = "Менеджер";
            }else if(employee instanceof SallesManager){
                type = "Продавец";
            }else if(employee instanceof Employee){
                type = "Сотрудник";
            }

            System.out.format(leftAlignFormat, employee.getId(),  employee.getSurname()+" "+employee.getName()+" "+employee.getPatronymic(), employee.getGender().getAbbreviation(), type, employee.getSalary() );
            System.out.format("+-------+"+borderName.toString()+"+--------+---------------+"+borderSalary.toString()+"+" + newLineMark);
        }

    }

    public static void save (Object obj, String file)    {
        try{
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(obj);
            oos.flush();
            fos.flush();
            oos.close();
            fos.close();
        }   catch(FileNotFoundException e){
            try{
                File f;
                f = new File(file);
                f.createNewFile();
                 save(obj, file);
            }   catch (IOException ee){
                ee.printStackTrace();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static Firm load(String file) {
        Firm firm;

        try{
            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            return (Firm) ois.readObject();
        }   catch(FileNotFoundException e){
//            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
             e.printStackTrace();
        }
        return new Firm(1000000.0);
    }
}
