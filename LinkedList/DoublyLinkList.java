package hometaskFirm.LinkedList;

/**
 * Created with IntelliJ IDEA.
 * User: sergey
 * Date: 11/6/13
 * Time: 10:50 AM
 * To change this template use File | Settings | File Templates.
 */

public class DoublyLinkList  extends LinkList {

    public static void main(String[] args) {
        DoublyLinkList list = new DoublyLinkList();

        list.addFirst(new DoublyLinkList.Element(new Integer(3)));
        list.addFirst(new DoublyLinkList.Element(new Integer(2)));
        list.addFirst(new DoublyLinkList.Element(new Integer(1)));
        list.addFirst(new DoublyLinkList.Element(new Integer(1)));
        list.addFirst(new DoublyLinkList.Element(new Integer(2)));
        list.addFirst(new DoublyLinkList.Element(new Integer(3)));
        list.addLast(new DoublyLinkList.Element(new Integer(8)));
        list.addFirst(new DoublyLinkList.Element(new Integer(8)));
        list.set(3, new DoublyLinkList.Element(999));
        list.set(6, new DoublyLinkList.Element(999));
//        list.print();
        System.out.println(list.toString());
        list.descendingIterator();
        System.out.println(list.toString());

        System.out.println(list.isPalindrome());

    }

    public void descendingIterator(){
        reverse = true;
    }


    public void addFirst( Element el){
        Element elements = (Element) this.getElements();
        if(elements == null) {
            this.setElements(el);
            this.setLastElement(el);
            return;
        }

        el.setNextElement(elements);
        elements.setPrevElement(el);
        el.setIndex(0);
        this.countTheIndex(elements);

        this.setElements(el);
    }

    public void addLast( Element el){
        Element last = (Element) this.getLastElement();
        el.setPrevElement(last);
        last.setNextElement(el);
        this.setLastElement(el);
    }

    public void set(int index, Element el){
        el.setIndex(index);
        index--;
        Element tmp;

        Element el1 = (Element) this.getElement(index);

        tmp = (Element) el1.getNextElement();
        this.countTheIndex(tmp);
        el.setNextElement(tmp);
        el1.setNextElement(el);

        el.setPrevElement(el1);
        tmp.setPrevElement(el);

    }


    @Override
    public String toString(){
        if(!reverse)
            return this.getElements().toString();
        return this.getLastElement().toString();
    }

    public boolean isPalindrome(){
        int size = this.size();

        if(size % 2 == 1)
            return false;
        Element  first = (Element) this.getElements();
        Element  last = (Element) this.getLastElement();
        for(int i = 0, c = size/2; i < c; i++){
            if(!first.getValue().equals(last.getValue())){
                return false;
            }
            first = (Element) first.getNextElement();
            last = last.getPrevElement();
        }

        return true;
    }

    static class Element <E extends Comparable> extends LinkList.Element{

        Element(E val) {
            super(val);
        }

        public void setPrevElement(Element el){
            this.prev = el;
        }

        public Element getPrevElement(){
            return this.prev;
        }

        @Override
        public String toString(){
            if(!reverse)
                return super.toString();

            if (this.getPrevElement() != null)
                return this.getValue()+"->"+this.getPrevElement();
            return this.getValue().toString();


        }

        private  Element prev;
    }


    private static boolean reverse = false;
}
