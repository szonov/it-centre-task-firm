package hometaskFirm.LinkedList;

import java.util.NoSuchElementException;

/**
 * Created with IntelliJ IDEA.
 * User: sergey
 * Date: 11/2/13
 * Time: 12:14 PM
 * To change this template use File | Settings | File Templates.
 */

public class LinkList {
    public static void main(String[] args) {
        LinkList list = new LinkList();

        list.addFirst(new LinkList.Element(new Integer(1)));
        list.addFirst(new LinkList.Element(new Integer(2)));
        list.addLast(new LinkList.Element(new Integer(3)));
        list.addLast(new LinkList.Element(new Integer(5)));
        list.addLast(new LinkList.Element(new Integer(6)));
        list.addLast(new LinkList.Element(true));
        list.addLast(new LinkList.Element(false));
        list.addLast(new LinkList.Element(2.344));
        list.set(2, new LinkList.Element(new Integer(7)));
        Element ell = list.getElement(2);
        list.print();
        System.out.println(list.toString());
    }

    public LinkList (){}

    public void addFirst( Element el){
        if(this.elements == null) {
            this.elements = el;
            this.lastElement = el;
            return;
        }

        el.setNextElement(this.elements);

        this.countTheIndex(this.elements);
        this.elements = el;
        this.elements.setIndex(0);
    }

    public void addLast( Element el){
        this.lastElement.setNextElement(el);
        this.lastElement = el;
    }

    public void set(int index, Element el){
        index--;
        LinkList.Element tmp;

        LinkList.Element el1 = this.getElement(index);
        el.setIndex(el1.getIndex()+1);

        tmp = el1.getNextElement();
        this.countTheIndex(tmp);
        el.setNextElement(tmp);

        el1.setNextElement(el);

    }

    public void print(){
        print(this.elements);
    }

    public int size(){
        return outerIndex;
    }

    public Element getElement(int index){
        if(LinkList.outerIndex < index -1 || index < 0)
            throw new NoSuchElementException("Element not found");

        return this.getElement(index, this.elements);
    }

    public Element getLastElement(){
        return this.lastElement;
    }

    protected Element getElements(){
        return this.elements;
    }

    protected void setElements(Element el){
       this.elements = el;
    }

    protected void setLastElement(Element el){
        this.lastElement = el;
    }

    @Override
    public String toString(){
        return this.elements.toString();
    }

    private void print(Element el){
        Object e = (el.getNextElement()!=null) ? "->"+el.getNextElement().getValue() : "  ";
        System.out.println(el.getValue()+""+e+" index ["+el.getIndex()+"]");

        if(el.getNextElement() != null)
            this.print(el.getNextElement());
    }

    void countTheIndex(Element elements){
        elements.setIndex(elements.getIndex()+1);
        if(elements.next != null  )
            countTheIndex(elements.getNextElement()) ;

    }

    private Element getElement(int index, Element el){
        if(el.getIndex() == index)
            return el;
        return this.getElement(index, el.getNextElement());
    }

    static class Element <E extends Comparable> {

        Element(E val){
            this.value = val;

            this.index = LinkList.outerIndex;

            LinkList.outerIndex++;
        }

        public E getValue (){
            return this.value;
        }

        public int getIndex(){
            return this.index;
        }

        public Element getNextElement(){
            return this.next;
        }

        public void setNextElement(Element el){
            this.next = el;
        }

        public void setIndex( int ind){
            this.index = ind;
        }

        public void setValue (E val){
            this.value = val;
        }

        @Override
        public String toString(){

            if (this.next != null)
                return this.value+"->"+this.next;
            return this.value.toString();
        }


        private E value;
        private Element next;
        private  int index;
    }

    private static int outerIndex = 0;
    private Element elements;
    private Element lastElement;
}

