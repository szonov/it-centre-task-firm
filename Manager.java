/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hometaskFirm;

import java.util.*;

/**
 *
 * @author sergey
 */
public class Manager extends Employee {
  public Manager(String aName, String aPatronymic, String aSurname, Gender aGender, Date aBirthday, Double aSalary, Department aDep){
    super(aName, aPatronymic, aSurname, aGender, aBirthday, aSalary );
    this.department = aDep;
  }
  
  @Override
  public String toString(){
    return super.toString()+this.getClass().getName()+" [department="+this.department.toString()
                           +"bonus="+this.bonus
                           +"subordinates="+this.subordinates.toString()
                           +"]";
  }
  
  public Department getDepartment(){
    return this.department;
  }
  
  public void addSubordinates(Employee worker){
    this.subordinates.add(worker);
  }

  @Override
  public void setSalaryAccount (Double salary){
      Double salaryAccount = super.getSlaryAccount();
      salaryAccount+= salary;
  }

  @Override
  public Double getSalary(){
      Double salary = super.getSalary()+(50*this.getSubordinates().size());
      return salary;
  }

  public ArrayList <Employee> getSubordinates(){
      return this.subordinates;
  }
  private Department department;
  private Double bonus;
  private  ArrayList <Employee> subordinates = new <Employee> ArrayList();
}
