/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hometaskFirm;

import java.io.Serializable;

/**
 *
 * @author sergey
 */
public class Department implements Serializable {
  public Department(String name){
    this.name = name;
  }
  
  public String toString(){
    return "[id=" +ID
            +"name="+name
            +"]";
  }

  public Integer getId(){
      return this.ID;
  }

  public String getName(){
      return this.name;
  }

  public void setId(Integer id){
      this.ID = id;
  }

  private Integer ID;
  private String name;
}
